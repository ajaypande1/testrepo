package test.automation.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

public class DriverHelper extends EventFiringWebDriver {
	private static RemoteWebDriver REAL_DRIVER = null;
	private static String testBrowser;

	private static final Thread CLOSE_THREAD = new Thread() {
		@Override
		public void run() {
			closeWebDriver();
		}
	};
	static {
		Runtime.getRuntime().addShutdownHook(CLOSE_THREAD);
		launchBrowser();
	}

	private DriverHelper() {
		super(REAL_DRIVER);
	}

	private static void launchBrowser() {
		testBrowser = Props.getBrowserName().toLowerCase();
		try {
			switch (testBrowser) {
			case ("chrome"):
				startChromeDriver();
				break;
			case ("firefox"):
				startFireFoxDriver();
				break;
			default:
				throw new IllegalArgumentException("Browser " + Params.BROWSER + " not supported");
			}
		} catch (IllegalStateException e) {
			System.out.println(e.toString());
		}
	}

	@SuppressWarnings("deprecation")
	public static WebDriver startChromeDriver() {
		DesiredCapabilities capabilities = getChromeDesiredCapabilities();

		if (Params.SELENIUM_HOST == null || Params.SELENIUM_HOST.isEmpty()) {
			REAL_DRIVER = new ChromeDriver(capabilities);
		} else {
			try {
				REAL_DRIVER = getRemoteWebDriver(capabilities);
			} catch (MalformedURLException e) {
				System.out.println(Params.SELENIUM_REMOTE_URL + " Error " + e.getMessage());
			}
		}
		clearCookies();
		maximize();
		clearCookies();
		return REAL_DRIVER;
	}

	private static DesiredCapabilities getChromeDesiredCapabilities() {
		LoggingPreferences logs = new LoggingPreferences();
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
		capabilities.setCapability(CapabilityType.LOGGING_PREFS, logs);
		ChromeOptions chromeOptions = new ChromeOptions();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		return capabilities;
	}

	@SuppressWarnings("deprecation")
	public static void startFireFoxDriver() {
		DesiredCapabilities capabilities = getFireFoxDesiredCapabilities();
		if (Params.SELENIUM_HOST == null || Params.SELENIUM_HOST.isEmpty()) {
			REAL_DRIVER = new FirefoxDriver(capabilities);
		} else {
			try {
				REAL_DRIVER = getRemoteWebDriver(capabilities);
			} catch (MalformedURLException e) {
				System.out.println(Params.SELENIUM_REMOTE_URL + " Error " + e.getMessage());
			}
		}
		clearCookies();
		maximize();
	}

	private static DesiredCapabilities getFireFoxDesiredCapabilities() {
		LoggingPreferences logs = new LoggingPreferences();
		logs.enable(LogType.DRIVER, Level.OFF);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setBrowserName("firefox");
		capabilities.setCapability("disable-restore-session-state", true);
		capabilities.setCapability("marionette", true);
		return capabilities;
	}

	public static void maximize() {
		REAL_DRIVER.manage().window().maximize();
	}

	public static void clearCookies() {
		REAL_DRIVER.manage().deleteAllCookies();
	}

	private static RemoteWebDriver getRemoteWebDriver(DesiredCapabilities capabilities) throws MalformedURLException {
		Params.SELENIUM_REMOTE_URL = "http://" + Params.SELENIUM_HOST + ":" + Params.SELENIUM_PORT + "/wd/hub";
		return new RemoteWebDriver(new URL(Params.SELENIUM_REMOTE_URL), (capabilities));
	}

	public static WebDriver getWebDriver() {
		return REAL_DRIVER;
	}

	public static void closeWebDriver() {
		System.out.println("Closing Webdriver");
		REAL_DRIVER.close();
		REAL_DRIVER.quit();
	}

	@Override
	public void close() {
		if (Thread.currentThread() != CLOSE_THREAD) {
			throw new UnsupportedOperationException(
					"You shouldn't close this WebDriver. It's shared and will close when the JVM exits.");
		}
		super.close();
	}
}
