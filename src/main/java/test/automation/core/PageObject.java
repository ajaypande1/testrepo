package test.automation.core;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import test.automation.core.DriverHelper;
import java.util.concurrent.TimeUnit;

public abstract class PageObject {
	private static final long DRIVER_WAIT_TIME = 40;
	protected static WebDriverWait wait;
	protected static WebDriver webDriver;

	static {
		webDriver = DriverHelper.getWebDriver();
		wait = new WebDriverWait(webDriver, DRIVER_WAIT_TIME);
	}

	protected static WebElement waitForExpectedElement(final By by) {
		return wait.until(visibilityOfElementLocated(by));
	}

	public static WebElement waitForExpectedElement(final By by, long waitTimeInSeconds) {
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, waitTimeInSeconds);
			return wait.until(visibilityOfElementLocated(by));
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
			return null;
		} catch (TimeoutException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	public static WebElement waitTillElementClickable(final By by) {
		try {
			WebDriverWait wait = new WebDriverWait(webDriver, 20000L);
			// wait.until(ExpectedConditions.presenceOfElementLocated(by));
			// wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			return wait.until(ExpectedConditions.elementToBeClickable(by));
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	private static ExpectedCondition<WebElement> visibilityOfElementLocated(final By by) throws NoSuchElementException {
		return driver -> {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
			}
			WebElement element = webDriver.findElement(by);
			return element.isDisplayed() ? element : null;
		};
	}

	public static String getText(final By by) {
		return waitForExpectedElement(by).getText();
	}

	public static String getText(WebElement element) {
		return element.getText();
	}

	public static String getAttributeValue(final By by, String value) {
		return waitForExpectedElement(by).getAttribute(value);
	}

	public static void click(final By by) {
		waitForExpectedElement(by).click();
	}

	public static boolean isElementPresent(final By by) {
		try {
			new WebDriverWait(webDriver, DRIVER_WAIT_TIME).until(ExpectedConditions.presenceOfElementLocated(by));

		} catch (TimeoutException exception) {
			System.out.println(exception.getMessage());
			return false;
		}
		return true;
	}

	protected static void clearEnterText(By by, String inputText) {
		waitForExpectedElement(by).clear();
		waitForExpectedElement(by).sendKeys(inputText);
	}

	protected static void clickEnter(By by, Keys inputText) {
		waitForExpectedElement(by).sendKeys(inputText);
	}

	public static void NavigateToURL(String NavigationURL) {
		webDriver.navigate().to(NavigationURL);
	}
	protected static void enterText(By by, String inputText) {
		waitForExpectedElement(by).sendKeys(inputText);
	}

	public static void click(By by, String text) {
		click(by);
		click(by, text);
	}

	public static void implicitwait() {
		webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	public static WebDriver getDriver() {
		return webDriver;
	}

}