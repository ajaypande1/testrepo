package test.automation.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import static java.lang.System.out;

public class Props {
	private static Properties environmentProps;
	private static Properties properties;

	public static String getProp(String key) {
		if ((key == null) || key.isEmpty()) {
			return "";
		} else {
			return properties.getProperty(key);
		}
	}

	public static String getSystemProp(String key) {
		if ((key == null) || key.isEmpty()) {
			return "";
		} else {
			return System.getProperty(key);
		}
	}

	public static String setSystemProp(String key, String val) {
		if ((key == null) || key.isEmpty()) {
			return "";
		} else {
			return System.setProperty(key, val);
		}
	}

	public static void loadRunConfigProps(String configPropertyFileLocation) {
		environmentProps = new Properties();
		try (InputStream inputStream = Props.class.getResourceAsStream(configPropertyFileLocation)) {
			environmentProps.load(inputStream);
			environmentProps.list(out);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		properties = new Properties();
		try (InputStream inputStream = Props.class.getResourceAsStream(environmentProps.getProperty("profile.path"))) {
			properties.load(inputStream);
			properties.list(out);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static String getBrowserName() {
		return System.getProperty("browser");
	}
}
