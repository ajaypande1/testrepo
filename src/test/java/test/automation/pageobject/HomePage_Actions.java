package test.automation.pageobject;

import org.openqa.selenium.By;
import test.automation.core.PageObject;

public class HomePage_Actions extends PageObject {

	public static By link_contact=By.linkText("Contact");
	public static By link_shop=By.linkText("Shop");

	public static void goto_Contact() {
		click(link_contact);		
	}
	
	public static void goto_Shop() {
		click(link_shop);		
	}
}
