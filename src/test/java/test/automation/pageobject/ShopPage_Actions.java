package test.automation.pageobject;

import org.openqa.selenium.By;
import test.automation.core.PageObject;

public class ShopPage_Actions extends PageObject {

	static By link_buy(String prodName) {
		return By.xpath(".//h4[text()='" + prodName + "']/ancestor::node()[2]/div/p/a");
	}
	static By link_cart=By.partialLinkText("Cart");	


	public static void click_buyProduct(String prodName) {
		click(link_buy(prodName));
	}
	
	public static void click_cart() {
		click(link_cart);
	}
}
