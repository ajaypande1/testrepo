package test.automation.step_definitions;

import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import test.automation.core.PageObject;
import test.automation.pageobject.ContactPage_Actions;
import test.automation.pageobject.HomePage_Actions;
import test.automation.pageobject.ShopPage_Actions;

public class App_StepDef {
	
	@Given("^I Open Jupier application$")
	public void i_Open_Jupier_application() throws Throwable {
		PageObject.NavigateToURL("https://jupiter.cloud.planittesting.com/");
	}

	@When("^I navigate to Contact page$")
	public void i_navigate_to_Contact_page() throws Throwable {
		HomePage_Actions.goto_Contact();
	}

	@When("^I click on Submit button$")
	public void i_click_on_Submit_button() throws Throwable {
		ContactPage_Actions.click_submit();
	}

	@When("^I validate the error message$")
	public void i_validate_the_error_message() throws Throwable {
		boolean flag=ContactPage_Actions.verify_ErrorMsg();
		assertTrue("Validating error msg exist",flag);
	}

	@When("^I enter valid data in the Contact page fields$")
	public void i_enter_valid_data_in_the_Contact_page_fields() throws Throwable {
		ContactPage_Actions.enter_ValidData();
	}

	@When("^validate the error message are gone$")
	public void validate_the_error_message_are_gone() throws Throwable {
		boolean flag=ContactPage_Actions.verify_ErrorMsg();
		assertTrue("Validating error msg NOT exist",!flag);
	}

	@Then("^I validate sucessful submission of message$")
	public void i_validate_sucessful_submission_of_message() throws Throwable {
		boolean flag=ContactPage_Actions.verify_ErrorMsg();
		assertTrue("Validating error msg NOT exist",!flag);
	}

	@When("^I enter invalid data in the Contact page fields$")
	public void i_enter_invalid_data_in_the_Contact_page_fields() throws Throwable {
		ContactPage_Actions.enter_InvalidData();
	}

	@When("^I navigate to Shop page$")
	public void i_navigate_to_Shop_page() throws Throwable {
		HomePage_Actions.goto_Shop();
	}

	@When("^I click on buy button to buy \"([^\"]*)\"$")
	public void i_click_on_buy_button_to_buy(String prodName) throws Throwable {
		ShopPage_Actions.click_buyProduct(prodName);
	}

	@Then("^I click on Cart button$")
	public void i_click_on_Cart_button() throws Throwable {
		ShopPage_Actions.click_cart();
	}

	@Then("^I verify items added earlier are in the cart$")
	public void i_verify_items_added_earlier_are_in_the_cart() throws Throwable {

	}
}
