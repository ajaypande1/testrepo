
Feature: Jupiter application test 

	Scenario: Contact page validation 
		Given I Open Jupier application 
		When I navigate to Contact page 
		And I click on Submit button 
		And I validate the error message 
		And I enter valid data in the Contact page fields 
		And validate the error message are gone
		
	Scenario: Contact page validation 
		Given I Open Jupier application 
		When I navigate to Contact page
		And I enter valid data in the Contact page fields 
		And I click on Submit button 
		Then I validate sucessful submission of message
		
	Scenario: Contact page validation 
		Given I Open Jupier application 
		When I navigate to Contact page 
		And I enter invalid data in the Contact page fields
		And I validate the error message  
		@jupiter 		
	Scenario: Shop page validation 
		Given I Open Jupier application 
		When I navigate to Shop page 
		And I click on buy button to buy "Fluffy Bunny" 
		And I click on buy button to buy "Fluffy Bunny" 
		And I click on buy button to buy "Funny Cow" 
		Then I click on Cart button 
		And I verify items added earlier are in the cart 
	    